﻿Imports HakamiLong

Module Module1

    Sub Main()
        Dim expression As String
        Dim answer As Long

        Console.WriteLine("Write your expression:")
        expression = Console.ReadLine()
        Try
            answer = Calculator.Calculate(expression)
        Catch ex As Exception
            Console.WriteLine("The exception was thrown: " + ex.ToString())
        End Try

        Console.WriteLine("The answer is: " + answer.ToString())
        Console.Read()

    End Sub

End Module
