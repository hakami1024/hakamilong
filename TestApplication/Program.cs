﻿using System;
using HakamiLong;

namespace TestApplication
{
    class Program
    {
        private static void Main(){
            Long[] longArray = new Long[ 10 ];
            Random random = new Random(DateTime.Now.Millisecond);
            for( int i = 0; i < 10; i++ )
                longArray[ i ] = random.Next(10);

            Console.WriteLine("Array in the beginning:");
            longArray.PrintArray();

            Console.WriteLine( "Array sorted default:" );
            Array.Sort( longArray );
            longArray.PrintArray();

            Console.WriteLine( "Array sorted descending:" );
            Array.Sort( longArray, Long.SORT_DESCENDING  );
            longArray.PrintArray();

            while( true ){
                Console.Write( "\nWrite your arifmetic expression.\nUse +, -, *, /, (), log2, sqrt:\n\t> " );
                String s = Console.ReadLine();
                String answer = Calculator.Calculate( s ).ToString();
                Console.WriteLine("Answer: "+answer);
            }
        }
    }

    static class LongExtensionPrint
    {
        internal static void PrintArray( this Long[] array ){
            for( int i=0; i<array.Length-1; i++ ){
                Console.Write("{0}, ", array[i]);
            }
            Console.WriteLine( "{0}.", array[ array.Length - 1 ] );
        }
    }
}
