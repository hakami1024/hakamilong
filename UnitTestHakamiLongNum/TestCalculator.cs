﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HakamiLong;

namespace UnitTestHakamiLongNum
{
    [TestClass]
    public class TestCalculator
    {
        [TestMethod]
        public void CalculateWithOperatorsOnly()
        {
            String s = "(15+3*6-4*3)*(8-2)";
            Long ans = Calculator.Calculate(s);
            Assert.AreEqual(126, ans);
        }

        [TestMethod]
        public void CalculateWithSqrtAndLog2()
        {
            String s = "(15+sqrt(9)*6-log2(16)*3)*(8-2)";
            Long ans = Calculator.Calculate(s);
            Assert.AreEqual(126, ans);

            String s1 = "(72 - sqrt(35+1))/log2(4)";
            Long ans1 = Calculator.Calculate(s1);
            Assert.AreEqual(33, ans1);

            String s2 = "7 - (1 - 2*log2(128)/3)";
            Long ans2 = Calculator.Calculate( s2 );
            Assert.AreEqual( 10, ans2 );
        }

        [TestMethod]
        public void gettingComponents()
        {
            Assert.AreEqual("8", Calculator.calculateWithoutBraces("5+3"));
            Assert.AreEqual("-12", Calculator.calculateWithoutBraces("-4+-4-4"));
        }
    }
}
