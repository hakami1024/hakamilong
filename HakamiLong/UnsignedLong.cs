﻿#define UNIT_TEST

using System;
using System.Linq;
using System.Text;
namespace HakamiLong
{
    public class UnsignedLong: IComparable<UnsignedLong>
    {
        
        public const int DIGITS_MAX_LEN = 2500;
        protected const int DIGIT_SIZE = 4;
        protected static readonly int DIGIT_BASE = (int)Math.Pow(10, DIGIT_SIZE);

        public int RealLen{ get; set; }
        /*
         * digits[RealLen-1] - начало числа
         * digits[i] - часть числа, не превышающая 10^DIGIT_SIZE
         * digits[0] - конец числа
         * */
        private readonly int[] digits = new int[DIGITS_MAX_LEN];
        
        ///////////////////////////////////////////////////////////////////////
        // Constructors and Clone
        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns new UnsignedLong that equals 0
        /// </summary>
        public UnsignedLong(){
            RealLen = 1;
        }

#if DEBUG
        public
#else
        private
#endif
        UnsignedLong(int[] digitsToRefer, int realLen)
        {
            digits = digitsToRefer;
            RealLen = realLen;
        }

        public UnsignedLong Clone()
        {
            int[] newdigits = new int[DIGITS_MAX_LEN];
            digits.CopyTo(newdigits, 0);
            return new UnsignedLong(newdigits, RealLen);
        }

        ///////////////////////////////////////////////////////////////////////
        // Casting, parsing
        ///////////////////////////////////////////////////////////////////////

        public static implicit operator UnsignedLong(int num)
        {
            int[] digits = new int[DIGITS_MAX_LEN];
            int len = 0;
            if( num == 0 )
                return new UnsignedLong();
            while( num > 0 ){
                digits[ len++ ] = num % DIGIT_BASE;
                num /= DIGIT_BASE;
            }
            UnsignedLong result = new UnsignedLong(digits, len);
            return result;
        }

        public static UnsignedLong Parse(string line)
        {
            UnsignedLong result = new UnsignedLong();
            String ex =  InvalidLongException.PARSING_EXCEPTION + line;

            int leadingZeroIndex = 0;
            while ( leadingZeroIndex < line.Length && line[ leadingZeroIndex ] == '0' )
                leadingZeroIndex++;

            StringBuilder s = new StringBuilder(line);
            if( leadingZeroIndex > 0 )
                s.Remove(0, Math.Min(leadingZeroIndex-1, s.Length-1));

            if (s.Length == 0)
                throw new FormatException(ex);

            result.RealLen = (int)Math.Ceiling(s.Length / (double)DIGIT_SIZE);

            int j = 0;
            while (s.Length > 0)
            {
                String part;
                if (s.Length > DIGIT_SIZE)
                {
                    char[] p = new char[DIGIT_SIZE];
                    s.CopyTo(s.Length - DIGIT_SIZE, p, 0, DIGIT_SIZE);
                    part = new String(p);
                }
                else part = s.ToString();

                int convertion = Convert.ToInt32(part);
                if (convertion < 0)
                    throw new InvalidLongException(ex);
                result.digits[j++] = convertion;

                if ( s.Length - DIGIT_SIZE > 0)
                    s.Remove(s.Length - DIGIT_SIZE, DIGIT_SIZE);
                else s = new StringBuilder();
            }

            return result;
        }

        public override string ToString()
        {
            StringBuilder digitsStr = new StringBuilder();

            for (int i = RealLen - 1; i >= 0; i--)
            {
                StringBuilder s = new StringBuilder( digits[i].ToString() );

                if( i != RealLen - 1 )
                    while( s.Length < DIGIT_SIZE )
                        s.Insert(0, "0");

                digitsStr.Append(s);
            }

            return digitsStr.ToString();
        }

        public static explicit operator String( UnsignedLong num ){
            return num.ToString();
        }

        public static explicit operator UnsignedLong(String num)
        {
            return Parse(num);
        }

        public static explicit operator int(UnsignedLong num)
        {
            if( num > int.MaxValue )
                throw new InvalidLongException(  InvalidLongException.INVALID_CASTING_EXCEPTION + ": num = "+num );

            int result = 0;
            int res = 0;
            int base10 = 1;
            int i = 0;
            while (i < num.RealLen)
            {
                result += res;
                res = num.digits[i] * base10;
                base10 *= DIGIT_BASE;
                i++;
            }

            return result + res;
        }

        ///////////////////////////////////////////////////////////////////////
        // Operators 
        ///////////////////////////////////////////////////////////////////////

        public static UnsignedLong operator +(UnsignedLong num1, UnsignedLong num2)
        {
            UnsignedLong result = new UnsignedLong();
            int len = Math.Max(num1.RealLen, num2.RealLen);

            int i, buf = 0;
            for (i = 0; i < len; i++)
            {
                int sum = num1.digits[i] + num2.digits[i] + buf;
                result.digits[i] = sum % DIGIT_BASE;
                buf = sum / DIGIT_BASE;
            }

            result.RealLen = i;
            result.digits[i] = buf;
            if (result.digits[i] != 0)
                result.RealLen++;

            return result;
        }

        public static UnsignedLong operator ++(UnsignedLong num)
        {
            return num + 1;
        }

        public static UnsignedLong operator -(UnsignedLong num1, UnsignedLong num2)
        {
            if (num2 > num1)
                throw new InvalidLongException( InvalidLongException.NEGATIVE_ANSWER_EXCEPTION);

            UnsignedLong result = new UnsignedLong();

            int len = num1.RealLen;

            int buf = 0;
            for (int i = 0; i < len; i++)
            {
                result.digits[i] += num1.digits[i] - num2.digits[i] - buf;
                buf = result.digits[i] < 0 ? 1 : 0;
                if (buf == 1)
                    result.digits[i] += DIGIT_BASE;
            }

            result.RealLen = len;
            while (result.digits[result.RealLen - 1] == 0 && result.RealLen > 0)
                result.RealLen--;

            return result;
        }

        public static UnsignedLong operator --(UnsignedLong num)
        {
            return num - 1;
        }

        public static UnsignedLong operator *(UnsignedLong num1, UnsignedLong num2)
        {
            UnsignedLong result = new UnsignedLong
            {
                RealLen = num1.RealLen + num2.RealLen
            };

            int buf = 0;
            for (int i = 0; i < num1.RealLen; i++)
            {
                int j;
                for (j = 0; j < num2.RealLen; j++)
                {
                    buf += result.digits[i + j] + num1.digits[i] * num2.digits[j];
                    result.digits[i + j] = buf % DIGIT_BASE;
                    buf /= DIGIT_BASE;
                }
                while (buf > 0)
                {
                    result.digits[i + j] += buf % DIGIT_BASE;
                    buf /= DIGIT_BASE;
                    j++;
                }
            }

            while (result.digits[result.RealLen - 1] == 0 && result.RealLen > 1)
                result.RealLen--;

            return result;
        }

        public static UnsignedLong operator /(UnsignedLong num1, UnsignedLong num2)
        {

            if( num1.RealLen < num2.RealLen )
                return 0;

            UnsignedLong left = 0;
            UnsignedLong right = num1;
            UnsignedLong result = num1.div2();
            UnsignedLong num1Delta = num1 - num2;

            while( true ) 
            {
                UnsignedLong mult = result*num2;

                int comparing = mult.CompareTo(num1);
                int comparingWithDelta = mult.CompareTo( num1Delta );
                int comparingResult = comparing + comparingWithDelta;


                if( comparing == 0 )
                    return result;
                if( comparingResult >= 1 ){
                    right = result;
                }
                else if( comparingResult <= -1 ){
                    left = result;
                }
                else return result;

                result = (right + left).div2();
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // Comparing
        ///////////////////////////////////////////////////////////////////////

        ///<summary>
        /// <para> 0 - if the Longs are Equal</para>
        /// <para> 1 - if this Long is bigger </para>
        /// <para> -1 - if the num is bigger</para>
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public int CompareTo(UnsignedLong num)
        {
            if (RealLen != num.RealLen)
            {
                return RealLen > num.RealLen ? 1 : -1;
            }

            for( int i=RealLen-1; i>=0; i-- )
                if (digits[i] != num.digits[i])
                {
                    return digits[i] > num.digits[i] ? 1 : -1;
                }

            return 0;
        }

        public static bool operator >(UnsignedLong num1, UnsignedLong num2)
        {
            return num1.CompareTo(num2) > 0;
        }

        public static bool operator >=(UnsignedLong num1, UnsignedLong num2)
        {
            return num1.CompareTo(num2) >= 0;
        }

        public static bool operator <(UnsignedLong num1, UnsignedLong num2)
        {
            return num1.CompareTo(num2) < 0;
        }

        public static bool operator <=(UnsignedLong num1, UnsignedLong num2)
        {
            return num1.CompareTo(num2) <= 0;
        }

        protected bool Equals( UnsignedLong other ) {
            return this.digits.SequenceEqual( other.digits ) && RealLen == other.RealLen;
        }

        public override int GetHashCode() {
            return ToString().GetHashCode();
        }
        
        public override bool Equals(object obj){
            if( ReferenceEquals( null, obj ) )
                return false;
            if( ReferenceEquals( this, obj ) )
                return true;
            if( obj.GetType() != GetType() )
                return false;
            return Equals( ( UnsignedLong ) obj );
        }
        

        public static bool operator ==(UnsignedLong num1, UnsignedLong num2){
            Object obj1 = num1;
            Object obj2 = num2;

            if ( obj1 == null )
                return obj2 == null;
            if ( obj2 == null )
                return false;

            return num1.CompareTo(num2) == 0;
        }

        public static bool operator !=(UnsignedLong num1, UnsignedLong num2)
        {
            Object obj1 = num1;
            Object obj2 = num2;

            if ( obj1 == null )
                return obj2 != null;
            if ( obj2 == null )
                return true;

            return num1.CompareTo(num2) != 0;
        }
       

        ///////////////////////////////////////////////////////////////////////
        // Special functions
        ///////////////////////////////////////////////////////////////////////

        public UnsignedLong Sqrt()
        {
            if (Equals(0))
                return 0;

            UnsignedLong left = 0;
            UnsignedLong right = Clone();

            while (left + 1 < right)
            {
                UnsignedLong result = (left + right).div2();
                int comparing = (result * result).CompareTo(this);

                switch (comparing)
                {
                    case 1:
                        right = result;
                        break;
                    case -1:
                        left = result;
                        break;
                    case 0:
                        return result;
                    default:
                        throw new InvalidLongException( InvalidLongException.COMPARISON_EXCEPTION);
                }
            }

            return left;
        }

        public UnsignedLong Log2()
        {
            if (this == 0)
                throw new InvalidLongException( InvalidLongException.LOG2_FROM_ZERO_EXCEPTION);

            UnsignedLong result = 0;
            UnsignedLong num = Clone();

            while (num != 0)
            {
                num = num.div2();
                result++;
            }
            
            return --result;
        }

#if DEBUG 
        public 
#else 
        private
#endif
        UnsignedLong div2()
        {
            UnsignedLong result = Clone();

            for (int i = RealLen-1; i >= 0; i--)
            {
                if ( (result.digits[i] % 2) == 1 && i>0 )
                    result.digits[i - 1] += DIGIT_BASE;

                result.digits[i] /= 2;
            }

            for (int i = RealLen-1; i > 0; i--)
                if (result.digits[i] == 0)
                    result.RealLen--;
                else break;

            return result;
        }
    }

    
 
}
