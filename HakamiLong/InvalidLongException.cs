﻿using System;

namespace HakamiLong
{    
    
    public class InvalidLongException: ApplicationException
    {
        public const String PARSING_EXCEPTION = "Некорректный ввод: ";
        public const String COMPARISON_EXCEPTION = "Поинтересуйтесь у Hakami, что творится в CompareTo в UnsignedLong";
        public const String LOG2_FROM_ZERO_EXCEPTION = "Двоичный логарифм нуля, бесконечность, не включена в библиотеку";
        public const String NEGATIVE_ANSWER_EXCEPTION = "Получено отрицательное число";
        public const String ROOT_FROM_NEGATIVE_EXCEPTION = "Попытка извлечь корень из отрицательного числа";
        public const String LOG2_FROM_NEGATIVE_EXCEPTION = "Попытка найти двоичный логарифм неположительного числа";
        public const String INVALID_CASTING_EXCEPTION = "Некорректное приведение типов";

        public InvalidLongException( String message )
            : base( message ){
        }

     }

}
