﻿using System;
using System.Collections.Generic;

[assembly: CLSCompliant( true )]
namespace HakamiLong
{
    public class SortDescending : IComparer<Long>
    {
        public int Compare( Long x, Long y ) {
            if ( x > y )
                return -1;
            return x < y ? 1 : 0;
        }
    }

    public class Long: IComparable<Long>
    {
        public static readonly SortDescending SORT_DESCENDING = new SortDescending();

        bool isPositive;
        public bool IsPositive
        {
            get { return this.isPositive; }
            set { this.isPositive = value; }
        }

        public UnsignedLong AbsValue{
            get { return this.absValue; }
        }

        private UnsignedLong absValue;

        ///////////////////////////////////////////////////////////////////////
        // Constructors and Clone
        ///////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Returns new Long that equals 0
        /// </summary>
        public Long() {
            this.isPositive = true;
            this.absValue = new UnsignedLong();
        }

        public Long(UnsignedLong absToRef, bool isPositive = true)
        {
            this.absValue = absToRef;
            this.isPositive = isPositive;
        }

        public Long Clone()
        {
            return new Long(this.AbsValue.Clone(), this.isPositive);
        }

        ///////////////////////////////////////////////////////////////////////
        // Casting
        ///////////////////////////////////////////////////////////////////////

        public static explicit operator Long(string line)
        {
            Long result = new Long();
            String s = line;

            if ( s[ 0 ] == '+' || s[ 0 ] == '-' ) {
                result.isPositive = s[ 0 ] == '+';
                s = s.Substring( 1, s.Length - 1 );
            }

            result.absValue = UnsignedLong.Parse( s );

            if ( result.AbsValue == 0 ) //-0, you shall not pass!
                result.isPositive = true;

            return result;
        }

        public static implicit operator Long(int num)
        {
            Long result = new Long();

            if( num < 0 ){
                result.isPositive = false;
                num *= -1;
            }
            result.absValue = num;

            return result;
        }

        public static explicit operator int(Long num)
        {
           return num.isPositive ? (int)num.AbsValue : -1 * ((int)num.AbsValue);
        }

        public static implicit operator Long(UnsignedLong num)
        {
            return new Long(num);
        }
        
        public override string ToString()
        {
            
           return (this.isPositive ? "" : "-") + this.AbsValue;
        }

        ///////////////////////////////////////////////////////////////////////
        // Comparing
        ///////////////////////////////////////////////////////////////////////

        public int CompareTo(Long num)
        {
            if (this.isPositive != num.isPositive)
            {
                return this.isPositive ? 1 : -1;
            }

            int absCompare = this.AbsValue.CompareTo( num.AbsValue );

            return this.isPositive ? absCompare : -absCompare;
        }

        public static bool operator >(Long num1, Long num2)
        {
            return num1.CompareTo( num2 ) > 0;
        }

        public static bool operator >=(Long num1, Long num2)
        {
            return num1.CompareTo(num2) >= 0;
        }

        public static bool operator <(Long num1, Long num2)
        {
            return num1.CompareTo(num2) < 0;
        }

        public static bool operator <= (Long num1, Long num2)
        {
            return num1.CompareTo(num2) <= 0;
        }


        public static bool operator ==( Long num1, Long num2 ){
            Object obj1 = num1;
            Object obj2 = num2;

            if( obj1 == null )
                return obj2 == null;
            if( obj2 == null )
                return false;

            return num1.CompareTo( num2 ) == 0;
        }

        public static bool operator !=( Long num1, Long num2 ) {
            Object obj1 = num1;
            Object obj2 = num2;

            if ( obj1 == null )
                return obj2 != null;
            if ( obj2 == null )
                return true;

            return num1.CompareTo( num2 ) != 0;
        }

        public override bool Equals(object obj){
            if( ReferenceEquals( null, obj ) )
                return false;
            if( ReferenceEquals( this, obj ) )
                return true;
            if( obj.GetType() != GetType() )
                return false;
            return Equals( ( Long ) obj );
        }

        protected bool Equals( Long other ) {
            return this.isPositive == other.isPositive  && this.AbsValue == other.AbsValue;
        }

        public override int GetHashCode() {
            return ( ToString() ).GetHashCode();
        }

        ///////////////////////////////////////////////////////////////////////
        // Operators 
        ///////////////////////////////////////////////////////////////////////

        public static Long operator +(Long num1, Long num2)
        {
            bool sameSign = (num1.isPositive == num2.isPositive);
            return sameSign ? summ(num1, num2) : sub(num1, num2);
        }

        public static Long operator ++(Long num)
        {
            return num + 1;
        }

        public static Long operator -(Long num1, Long num2)
        {
            bool sameSign = num1.isPositive == num2.isPositive;
            return sameSign ? sub(num1, num2) : summ(num1, num2);
        }

        public static Long operator --(Long num)
        {
            return num - 1;
        }

        private static Long sub(Long num1, Long num2)
        {
            int absCompare = num1.AbsValue.CompareTo(num2.AbsValue);

            switch (absCompare)
            {
                case 1:
                    Long result = num1.AbsValue - num2.AbsValue;
                    result.isPositive = num1.isPositive;
                    return result;
                case -1:
                    result = num2.AbsValue - num1.AbsValue;
                    result.isPositive = !num1.isPositive;
                    return result;
                case 0:
                    return new Long();
                default:
                    throw new InvalidLongException( InvalidLongException.COMPARISON_EXCEPTION);
            }
        }

        private static Long summ(Long num1, Long num2)
        {
            Long result = num1.AbsValue + num2.AbsValue;
            result.isPositive = num1.isPositive;
            return result;
        }

        public static Long operator *(Long num1, Long num2)
        {
            Long result = num1.AbsValue * num2.AbsValue ;
            result.IsPositive = (num1.IsPositive == num2.IsPositive);
            return result;
        }

        public static Long operator /(Long num1, Long num2)
        {
            Long result = num1.AbsValue/num2.AbsValue;
            result.isPositive = (result == 0) || (num1.isPositive == num2.isPositive );
            return result;
        }

        ///////////////////////////////////////////////////////////////////////
        // Special functions
        ///////////////////////////////////////////////////////////////////////

        public Long Sqrt()
        {
            if (!this.isPositive)
                throw new InvalidLongException( InvalidLongException.ROOT_FROM_NEGATIVE_EXCEPTION);

            return this.AbsValue.Sqrt();
        }

        public Long Log2()
        {
            if (this.isPositive && this.AbsValue != 0)
                return this.AbsValue.Log2();
            throw new InvalidLongException( InvalidLongException.LOG2_FROM_NEGATIVE_EXCEPTION);
        }

    }

}
